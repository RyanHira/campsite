<?php
/**Created by Robin Knol**/
class AdminModel
{
//    __contruct is used to make a general database connection using config.php.
    public function __construct()
    {
        $this->conn = Connection::make(App::get('config')['database']);
    }

//    selectAll is used to find all withing the database
    public function selectAll()
    {
//        set-up query for all users from database
        $query = "SELECT * FROM admin";
//        Prepare the query
        $stmt = Connection::make(App::get('config')['database'])->prepare($query);
//        Execute the query
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);
//        return all data
        return $stmt->fetchAll();
    }

    public function get($id)
    {
//        set-up query with id as parameter
        $query = "SELECT * FROM admin where id = :id";
//        Prepare the query
        $stmt = Connection::make(App::get('config')['database'])->prepare($query);
//        Bind parameters
        $stmt->bindParam(':id', $id);
//        execute the query
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);
//        return the fetched data
        return $stmt->fetch();
    }


//    Create function, with setting the values to be added to the database.
    public function create($id, $adminName, $pwd, $adminEmail)
    {
//        set-up query and setting up the values that will be added to the database
        $query = "INSERT INTO admin (id, adminName, pwd, adminEmail, created_at)
         VALUES (:id, :adminName,:pwd,:adminEmail, date(now()))";
//        prepare the query
        $stmt = $this->conn->prepare($query);
//        Bind the parameters
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':adminName', $adminName);
        $stmt->bindParam(':pwd', $pwd);
        $stmt->bindParam(':adminEmail', $adminEmail);

//        execute the statement
        $stmt->execute();

    }

//    Update function with parameters
    public function update($id, $adminName, $pwd, $adminEmail)
    {
//      setting up the query
        $query = "UPDATE admin SET adminName=:adminName, pwd=:pwd, adminEmail=:adminEmail WHERE id = :id";
//        perparing the query
        $stmt = $this->conn->prepare($query);
//        binding parameters
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':adminName', $adminName);
        $stmt->bindParam(':pwd', $pwd);
        $stmt->bindParam(':adminEmail', $adminEmail);
//        execute the statement
        $stmt->execute();
    }

    public function delete($id)
    {

        $query = "DELETE FROM admin WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

    }
}