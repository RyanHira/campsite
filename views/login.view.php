<?php
/**
 * Created by: Robin Kaarsgaren 2022
 **/
require 'inc/nav.php';
?>
<!DOCTYPE html>
<html lang="en">

<body>
<!-- Page Header-->
<header class="masthead" style="background-image: url('../images/head.jpg')">
    <div class="container position-relative px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <div class="site-heading">
                    <h1>Camping</h1>
                    <span class="subheading">De beste camping van Nederland! </span>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main Content-->
<?php require 'controllers/SessionV2.php'; ?>

</body>

<?php
require 'inc/footer.php';
?>
</html>
