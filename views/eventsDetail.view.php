<?php
/**made by Ryan hira**/
require 'inc/nav.php';
?>
<!DOCTYPE html>
<html lang="en">

<body>
<!-- Page Header-->
<div class="container" style="margin-top: 75px; margin-bottom: 200px;">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <a href="evenementen" class="btn btn-success">Back</a>
                </div>
                <div class="card-body">


                    <?php

                    $eventModel = new EventModel();
                    $currentEvent = $eventModel->get($_GET['id']);
                    ?>

                    <div class="row gx-4 gx-lg-5 justify-content-center">
                        <div class="col-md-10 col-lg-8 col-xl-7">

                            <!-- Main Content-->

                            <!-- Post preview-->
                            <?php
                            $eventModel = new EventModel();
                            $currentEvent = $eventModel->get($_GET['id']);
                            ?>
                            <div class="post-preview">
                                <h2 class="post-title"><?= $currentEvent->name; ?></h2>
                                <h3 class="post-subtitle"> <?= $currentEvent->locatie; ?></h3>
                                <p><?= $currentEvent->description; ?></p>

                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


</body>

<?php
require 'inc/footer.php';
?>
</html>
