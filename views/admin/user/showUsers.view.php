<?php
/**created by robin knol and Robin kaarsgaren**/
include('./views/inc/footer.php');
include('./views/inc/navAdmin.php');


?>

<!--standard container bootstrap-->
<div class="container" style="margin-top: 75px; margin-bottom: 200px;">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <h3>Camping Data</h3>
                    <!--                        button to go to add klant php page-->
                    <a href="/createUser" class="btn btn-success">voeg een klant toe</a>
                    <a href="/admin" class="btn btn-primary">Back</a>
                </div>
                <div class="card-body">
                    <table class="table table-users table-bordered table-striped">
                        <thead>
                        <!--                        create a list, showing the data-->
                        <tr>
                            <th>ID</th>
                            <th>Voornaam</th>
                            <th>Achternaam</th>
                            <th>telefoon nummer</th>
                            <th>email</th>
                            <th>created_at</th>
                            <th>updated_at</th>
                            <th>deleted_at</th>
                            <th>edit</th>

                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $User = new UserModel();
                        //----------------------fOREACH LOOP INTO $ROW TO SHOW EACH DATABASE ITEM AS $ROW->XX----------------------------------
                        $result = $User->selectAll();
                        if ($result) {
                            foreach ($result as $row) {
                                ?>
                                <tr>
                                    <td><?= $row->id; ?></td>
                                    <td><?= $row->voornaam; ?></td>
                                    <td><?= $row->achternaam; ?></td>
                                    <td><?= $row->telnummer; ?></td>
                                    <td><?= $row->email; ?></td>
                                    <td><?= $row->created_at; ?></td>
                                    <td><?= $row->updated_at; ?></td>
                                    <td><?= $row->deleted_at; ?></td>
                                    <?php
                                    if (is_null($row->deleted_at)) {
                                        ?>
                                        <td>
                                            <!--                                FETCHING EDIT PAGE THROUGH ROUTES.PHP FROM THE ROW WHERE THE ID'S MATCH-->
                                            <a href="/updateUser?id=<?= $row->id; ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                        <td>
                                            <!-------------------FORM ACTION CODE FETCHES _POST FUNCTION FROM CODE.VIEW.PHP-->
                                            <form action="/deleteUser" method="POST">
                                                <button type="submit" name="id" value="<?= $row->id; ?>"
                                                        class="btn btn-danger">Delete
                                                </button>
                                            </form>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                        } else {
                            echo "No Record Found";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
