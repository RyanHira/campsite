<?php
/**
 * Created by: Robin Kaarsgaren 2022
 **/
class RegisterController
{
    private $userModel;

    public function __construct()
    {
        $this->userModel = new UserModel();
    }

    public function register()
    {
        // Get data from the form
        $voornaam = $_POST['voornaam'];
        $achternaam = $_POST['achternaam'];
        $pwd = password_hash($_POST['pwd'], PASSWORD_DEFAULT);
        $telnummer = $_POST['telnummer'];
        $email = $_POST['email'];



        // Validate data
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Return error message
        }

        // Insert data into the database
        $this->userModel->create($voornaam, $achternaam, $pwd, $telnummer, $email);

        header('login');
    }
    //it took a bit, and thanks to Robin Knol's new router version it magically worked
}
