# Created by Robin Knol
create table users
(
    id         int primary key not null auto_increment,
    voornaam   varchar(256)    not null,
    achternaam      varchar(256)    not null,
    pwd        varchar(256)    not null,
    telnummer       int(10)     not null,
    email varchar(256) not null,
    created_at datetime default current_timestamp,
    updated_at datetime default current_timestamp,
    deleted_at datetime default null
);
CREATE TABLE `booking` (
                           `id` int primary key not null auto_increment,
                           `Datum_van` datetime NOT NULL,
                           `Datum_Tot` datetime NOT NULL,
                           `mensen` int(10) NOT NULL,
                           `caravan` int(10) NOT NULL,
                           `tent` int(10) NOT NULL,
                           `camper` int(10) NOT NULL,
                           `auto` int(10) NOT NULL,
                           `dieren` int(10) NOT NULL,
                           `kinderen` int(10) NOT NULL
);

CREATE TABLE `events`
(
    `id`          int(11)      primary key not null auto_increment,
    `name`        varchar(255) NOT NULL,
    `description` text         NOT NULL,
    `locatie`     varchar(100) NOT NULL,
    `updated_at`  timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created_at`  timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at`  datetime              default null
);
create table admin
(
    id         int primary key not null auto_increment,
    adminName   varchar(256)    not null,
    pwd        varchar(256)    not null,
    adminEmail varchar(256) not null,
    created_at datetime default current_timestamp,
    updated_at datetime default current_timestamp,
    deleted_at datetime default null
);
insert into admin (adminName, pwd, adminEmail, created_at, updated_at, deleted_at)
values ('admin','123','admin@mail.com', date(now()),date(now()), null);
