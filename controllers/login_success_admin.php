<?php
/**
 * Created by: Robin Kaarsgaren 2022
 **/
session_start();
//checks if session exists with adminName
if (isset($_SESSION["adminName"])) {
    echo '<p>welkom '.$_SESSION["adminName"].'</p>';
    echo '<h1>Demo login admin success</h1>';

    header('location: admin'); //pas locatie aan naar dashboard

    //else tells you that you are on restricted area and automatically kicked to the login for admin
} else {
    echo '<div class="dropdown">
        <h1>Deze pagina is alleen voor personeel!</h1>
</div>';

    //gives 3 seconds before you get thrown back to the login page for admins
    header("refresh:3; url=camping-management"); //not sure yet on naming


}
