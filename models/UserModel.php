<?php

/**
 * Created by Robin knol
 **/

class UserModel
{

//    __contruct is used to make a general database connection using config.php.
    public function __construct()
    {
        $this->conn = Connection::make(App::get('config')['database']);
    }

//    selectAll is used to find all withing the database
    public function selectAll()
    {
//        set-up query for all users from database
        $query = "SELECT * FROM users";
//        Prepare the query
        $stmt = Connection::make(App::get('config')['database'])->prepare($query);
//        Execute the query
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);
//        return all data
        return $stmt->fetchAll();
    }

    public function get($id)
    {
//        set-up query with id as parameter
        $query = "SELECT * FROM users where id = :id";
//        Prepare the query
        $stmt = Connection::make(App::get('config')['database'])->prepare($query);
//        Bind parameters
        $stmt->bindParam(':id', $id);
//        execute the query
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);
//        return the fetched data
        return $stmt->fetch();
    }


//    Create function, with setting the values to be added to the database.
    public function create($voornaam, $achternaam, $pwd, $telnummer, $email)
    {
//        set-up query and setting up the values that will be added to the database
        $query = "INSERT INTO users (voornaam, achternaam, pwd, telnummer, email, created_at)
         VALUES ( :voornaam,:achternaam, :pwd, :telnummer, :email, date(now()))";
//        prepare the query
        $stmt = $this->conn->prepare($query);
//        Bind the parameters
//        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':voornaam', $voornaam);
        $stmt->bindParam(':achternaam', $achternaam);
        $stmt->bindParam(':pwd', $pwd);
        $stmt->bindParam(':telnummer', $telnummer);
        $stmt->bindParam(':email', $email);
//        execute the statement
        $stmt->execute();

    }

//    Update function with parameters
    public function update($id, $voornaam, $achternaam, $pwd, $telnummer, $email)
    {
//      setting up the query
        $query = "UPDATE users SET voornaam=:voornaam, achternaam=:achternaam, pwd=:pwd , telNummer=:telnummer, email=:email WHERE id = :id";
//        perparing the query
        $stmt = $this->conn->prepare($query);
//        binding parameters
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':voornaam', $voornaam);
        $stmt->bindParam(':achternaam', $achternaam);
        $stmt->bindParam(':pwd', $pwd);
        $stmt->bindParam(':telnummer', $telnummer);
        $stmt->bindParam(':email', $email);
//        execute the statement
        $stmt->execute();
    }

    public function delete($id)
    {

        $query = "UPDATE users SET deleted_at = NOW() WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

    }

}