<?php
/**created by Jaymian Nawang and Robin Knol**/
class BookingModel
{


    public function __construct()
    {
        $this->conn = Connection::make(App::get('config')['database']);
    }

    public function selectAll()
    {
        $query = "SELECT * FROM booking";
        $stmt = Connection::make(App::get('config')['database'])->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $stmt->fetchAll();
    }

    public function get($id)
    {
        $query = "SELECT * FROM booking where id = :id";
        $stmt = Connection::make(App::get('config')['database'])->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $stmt->fetch();
    }

    public function create( $Datum_van, $Datum_tot, $mensen, $caravan, $tent, $camper, $auto, $dieren, $kinderen )
    {

        $query = "INSERT INTO booking (Datum_van, Datum_tot, mensen, caravan, tent,camper,auto,dieren,kinderen)
             VALUES ( :Datum_van,:Datum_tot,:mensen, :caravan, :tent, :camper, :auto, :dieren, :kinderen )";
        $stmt = $this->conn->prepare($query);
//        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':Datum_van', $Datum_van);
        $stmt->bindParam(':Datum_tot', $Datum_tot);
        $stmt->bindParam(':mensen', $mensen);
        $stmt->bindParam(':caravan', $caravan);
        $stmt->bindParam(':tent', $tent);
        $stmt->bindParam(':camper', $camper);
        $stmt->bindParam(':auto', $auto);
        $stmt->bindParam(':dieren', $dieren);
        $stmt->bindParam(':kinderen', $kinderen);

        $stmt->execute();

    }

    public function update($id,$Datum_van, $Datum_tot, $mensen, $caravan, $tent, $camper, $auto, $dieren, $kinderen ){
        $query = "UPDATE booking  SET Datum_van=:Datum_van, Datum_tot=:Datum_tot, mensen=:mensen, caravan=:caravan, tent=:tent, camper=:camper,  auto=:auto, dieren=:dieren, kinderen=:kinderen   WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':Datum_van', $Datum_van);
        $stmt->bindParam(':Datum_tot', $Datum_tot);
        $stmt->bindParam(':mensen', $mensen);
        $stmt->bindParam(':caravan', $caravan);
        $stmt->bindParam(':tent', $tent);
        $stmt->bindParam(':camper', $camper);
        $stmt->bindParam(':auto', $auto);
        $stmt->bindParam(':dieren', $dieren);
        $stmt->bindParam(':kinderen', $kinderen);
        $stmt->execute();

    }

    public function delete($id)
    {

        $query = "DELETE FROM booking WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

    }



}