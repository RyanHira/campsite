<?php
/**
 * Created by: Ryan Hira 2022
 * untitled
 */

//Get routes algemeen
$router->get( '','PagesController@home');
$router->get('teacher', 'TeacherController@index');
$router->get('about', 'PagesController@about');
$router->get('post', 'PagesController@post');
$router->get('contact', 'PagesController@contact');
$router->get('admin', 'PagesController@admin');
$router->get('evenementen', 'PagesController@evenementen');
$router->get('eventsDetail', 'PagesController@eventsDetail');

//register van Robin Kaarsgaren
$router->get('registerPage', 'PagesController@registerPage');
$router->get('registerHandler', 'PagesController@registerHandler');
$router->post('register', 'RegisterController@register');

//login van Robin Kaarsgaren
$router->get('login', 'PagesController@login');
$router->get('session', 'PagesController@session');
$router->get('loginSuccess', 'PagesController@loginSuccess');

//admin login van Robin Kaarsgaren
$router->get('AdminLogin', 'PagesController@AdminLogin');
$router->get('loginSuccessAdmin', 'PagesController@loginSuccessAdmin');

//Get routes user admin
$router->get('adminUser', 'PagesController@adminUser', 'UserController');
$router->get('createUser', 'PagesController@createUser');
$router->get('updateUser', 'PagesController@updateUser');

//Get routes Admin admin
$router->get('createAdmin','PagesController@createAdmin');
$router->get('editAdmin', 'PagesController@updateAdmin');
$router->get('showAdmin', 'PagesController@showAdmin', 'AdminController');

//get voor de events
$router->get('createEvent','PagesController@createEvent');
$router->get('editEvent', 'PagesController@editEvent');
$router->get('adminEvent', 'PagesController@adminEvent', 'EventController');

//get routes booking jaymian
$router->get('createEvent','PagesController@createEvent');
$router->get('editEvent', 'PagesController@editEvent');
$router->get('adminEvent', 'PagesController@adminEvent', 'EventController');


//Get routes Booking admin
$router->get('createBookingAdmin','PagesController@createBookingAdmin');
$router->get('editBookingAdmin', 'PagesController@updateBookingAdmin');
$router->get('adminBooking', 'PagesController@adminBooking', 'BookingAdminController');

//Get routes event admin
$router->get('createEventAdmin','PagesController@createEventAdmin');
$router->get('editEventAdmin', 'PagesController@updateEventAdmin');
$router->get('adminEvent', 'PagesController@adminEvent', 'EventController');

//Get routes Reservering admin
$router->get('adminReservering', 'PagesController@admin', 'EventController');
$router->get('createReserveringAdmin', 'PagesController@createEvent');
$router->get('updateReserveringAdmin', 'PagesController@updateEvent');

//register
$router->post('registerPage', 'PagesController@registerPage');
$router->post('registerHandler', 'PagesController@registerHandler');
$router->post('register', 'RegisterController@register');

//login
$router->post('login', 'PagesController@login');
$router->post('session', 'PagesController@session');
$router->post('loginSuccess', 'PagesController@loginSuccess');
$router->post('', 'PagesController@home');

//admin login
$router->post('AdminLogin', 'PagesController@AdminLogin');
$router->post('loginSuccessAdmin', 'PagesController@loginSuccess');

//Post routes user
$router->post('updateUser', 'UserController@update');
$router->post('createUser', 'UserController@create');
$router->post('deleteUser', 'UserController@delete');

//Post routes booking jaymian
$router->post('editBooking', 'BookingController@update');
$router->post('deleteBooking', 'BookingController@delete');
$router->post('createBooking', 'BookingController@create');

//Post routes booking admin
$router->post('updateBooking', 'BookingAdminController@update');
$router->post('createBooking', 'BookingAdminController@create');
$router->post('deleteBooking', 'BookingAdminController@delete');

//Post routes events
$router->post('editEvent', 'EventController@update');
$router->post('createEvent', 'EventController@create');
$router->post('deleteEvent', 'EventController@delete');

//Post routes admin
$router->post('editAdmin', 'AdminController@update');
$router->post('createAdmin', 'AdminController@create');
$router->post('deleteAdmin', 'AdminController@delete');
