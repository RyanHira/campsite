<?php
/**
 * Created by: Robin Kaarsgaren 2022
 **/
class LoginController {
    protected $userModel;
    protected $session;

    public function __construct(UserModel $userModel, Session $session) {
        $this->userModel = $userModel;
        $this->session = $session;
    }

    public function login() {
        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $user = $this->userModel->getUserByEmail($email);

            if (!$user) {
                $this->session->set('error', 'Invalid email or password');
                return redirect('login');
            }

            if (password_verify($password, $user['password'])) {
                $this->session->set('user', $user);
                return redirect('home');
            } else {
                $this->session->set('error', 'Invalid email or password');
                return redirect('login');
            }
        }
    }
}
