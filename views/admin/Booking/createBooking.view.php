<?php
/**created by Jaymian Nawang and Robin Knol**/
include('./views/inc/footer.php');
include('./views/inc/navAdmin.php');
?>

<div class="container" style="margin-top: 75px; margin-bottom: 200px;">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <h3>Camping reserveren</h3>
                    <a href="/adminBooking" class="btn btn-primary">Back</a>
                </div>
                <div class="card-body">
                    <h1>User create</h1>

                    <form action="/createBooking" method="POST">

                        <div class="mb-3">
                            <label for="Datum_van" class="form-label">Datum_van</label>
                            <input type="date" name="Datum_van" required class="form-control" id="Datum_van">
                        </div>
                        <div class="mb-3">
                            <label for="Datum_Tot" class="form-label">Datum_Tot</label>
                            <input type="date" name="Datum_Tot" required class="form-control" id="Datum_Tot">
                        </div>
                        <div class="mb-3">
                            <label for="mensen" class="form-label">Mensen</label>
                            <input type="text" name="mensen" required class="form-control" id="mensen">
                        </div>

                        <div class="mb-3">
                            <label for="caravan" class="form-label">caravan</label>
                            <input type="text" name="caravan" required class="form-control" id="caravan">
                        </div>

                        <div class="mb-3">
                            <label for="tent" class="form-label">tent</label>
                            <input type="text" name="tent" required class="form-control" id="tent">
                        </div>

                        <div class="mb-3">
                            <label for="camper" class="form-label">camper</label>
                            <input type="text" name="camper" required class="form-control" id="camper">
                        </div>

                        <div class="mb-3">
                            <label for="auto" class="form-label">auto</label>
                            <input type="text" name="auto" required class="form-control" id="auto">
                        </div>

                        <div class="mb-3">
                            <label for="dieren" class="form-label">dieren</label>
                            <input type="text" name="dieren" required class="form-control" id="dieren">
                        </div>

                        <div class="mb-3">
                            <label for="kinderen" class="form-label">kinderen</label>
                            <input type="text" name="kinderen" required class="form-control" id="kinderen">
                        </div>

                        <!--                            <div class="mb-3">-->
                        <!--                                <label for="reservering_ID" class="form-label">reservering_ID</label>-->
                        <!--                                <input type="text" name="reservering_ID" required class="form-control" id="reservering_ID">-->
                        <!--                            </div>-->
                </div>


                <button type="submit" class="btn btn-primary">Submit</button>
                </form>


            </div>
        </div>
    </div>
</div>
</div>


