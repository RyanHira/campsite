<?php

/**created by Robin Knol**/
include('./views/inc/footer.php');
include('./views/inc/navAdmin.php');
?>
<div class="container" style="margin-top: 75px; margin-bottom: 200px;">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <h3>admin Toevoegen</h3>
                    <a href="/showAdmin" class="btn btn-primary">Back</a>
                </div>
                <div class="card-body">
                    <h1>Maak een admin aan!</h1>
                    <form action="/createAdmin" method="POST">
                        <div class="mb-3">
                            <label for="adminName" class="form-label">Admin Name</label>
                            <input type="text" name="adminName" required class="form-control" id="adminName">
                        </div>
                        <div class="mb-3">
                            <label for="pwd" class="form-label">password</label>
                            <input type="password" name="pwd" required class="form-control" id="pwd">
                        </div>
                        <div class="mb-3">
                            <label for="adminEmail" class="form-label">Email</label>
                            <input type="text" name="adminEmail" required class="form-control" id="adminEmail">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
