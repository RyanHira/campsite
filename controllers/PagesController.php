<?php
//
//include('./views/inc/footer.php');
//include('./views/inc/nav.php');
class PagesController
{
    //algemeen
    public function home()
    {
        require 'views/index.view.php';
    }

    public function post()
    {
        require 'views/post.view.php';
    }

    public function evenementen()
    {
        require 'views/evenementen.view.php';
    }

    public function eventsDetail()
    {
        require 'views/eventsDetail.view.php';
    }

    public function contact()
    {
        require 'views/contact.view.php';
    }
//admin
    public function admin()
    {
        require 'views/admin/Admin.view.php';
    }
// login
    public function registerPage()
    {
        require 'views/register.view.php';
    }

    public function registerHandler()
    {
        require 'controllers/RegisterController.php';
    }

    public function login()
    {
        require 'views/login.view.php';
    }

    public function session_patcher()
    {
        require 'controllers/session_patcher.php';
    }

    public function loginSuccess()
    {
        require 'controllers/login_success.php';
    }

    public function session()
    {
        require 'controllers/SessionV2.php';
    }
//admin login
    public function AdminLogin()
    {
        require 'views/adminLogin.view.php';
    }

    public function loginSuccessAdmin()
    {
        require 'controllers/login_success_admin.php';
    }

//admin admin panel
    public function createAdmin()
    {
        require 'views/admin/admin/createAdmin.view.php';
    }

    public function updateAdmin()
    {
        require 'views/admin/admin/updateAdmin.view.php';
    }

    public function showAdmin()
    {
        require 'views/admin/admin/showAdmin.view.php';
    }

//    USER ADMIN
    public function adminUser()
    {
        require 'views/admin/user/showUsers.view.php';
    }

    public function createUser()
    {
        require 'views/admin/user/createUser.view.php';
    }

    public function updateUser()
    {
        require 'views/admin/user/updateUser.view.php';
    }

//    BOOKING ADMIN
    public function adminBooking()
    {
        require 'views/admin/Booking/showBooking.view.php';
    }

    public function createBookingAdmin()
    {
        require 'views/admin/Booking/createBooking.view.php';
    }

    public function updateBookingAdmin()
    {
        require 'views/admin/Booking/updateBooking.view.php';
    }

//    EVENT ADMIN
    public function adminEvent()
    {
        require 'views/admin/event/showEvents.view.php';
    }


    public function createEventAdmin()
    {
        require 'views/admin/event/createEvent.view.php';
    }

    public function updateEventAdmin()
    {
        require 'views/admin/event/updateEvent.view.php';
    }

//    PLEKKEN ADMIN
    public function adminPlek()
    {
        require 'views/admin/plekken/showPlekken.view.php';
    }

    public function createPlekAdmin()
    {
        require 'views/admin/plekken/createPlek.view.php';
    }

    public function updatePlekAdmin()
    {
        require 'views/admin/plekken/updatePlek.view.php';
    }

//routes jaymian booking

    public function createBooking()
    {
        require 'views/post.view.php';
    }


    public function booking()
    {
        require 'views/profile.view.php';
    }

    public
    function editBooking()
    {
        require 'views/editBooking.view.php';
    }

}