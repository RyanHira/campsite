<?php

/**
 * Created by: Stephan Hoeksema 2022
 * routing
 */

class Request
{
    public static function uri()
    {
        return trim($_SERVER['REQUEST_URI'], '/');
    }

    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

}