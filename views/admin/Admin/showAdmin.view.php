<?php
/**created by robin knol**/
include('./views/inc/footer.php');
include('././views/inc/navAdmin.php');

?>

    <!--standard container bootstrap-->
    <div class="container" style="margin-top: 100px; margin-bottom: 200px;>
    <div class=" col-md-12 mt-4">
    <div class="card">
        <div class="card-header">
            <h3>Administrators</h3>
            <a href="/createAdmin" class="btn btn-success">voeg een admin toe</a>
            <a href="/admin" class="btn btn-primary">Back</a>
        </div>
        <div class="card-body">
            <table class="table table-users table-bordered table-striped">
                <thead>
                <!--                        create a list, showing the data-->
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $Admin = new AdminModel();
                //----------------------fOREACH LOOP INTO $ROW TO SHOW EACH DATABASE ITEM AS $ROW->XX----------------------------------
                $result = $Admin->selectAll();
                if ($result) {
                    foreach ($result as $Admin) {
                        ?>
                        <tr>
                        <td><?= $Admin->id; ?></td>
                        <td><?= $Admin->adminName; ?></td>
                        <td><?= $Admin->adminEmail; ?></td>
                        <?php
                        ?>
                        <td>
                            <!--                                FETCHING EDIT PAGE THROUGH ROUTES.PHP FROM THE ROW WHERE THE ID'S MATCH-->
                            <a href="/editAdmin?id=<?= $Admin->id; ?>" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <!-------------------FORM ACTION CODE FETCHES _POST FUNCTION FROM CODE.VIEW.PHP-->
                            <form action="deleteAdmin" method="POST">
                                <button type="submit" name="id" value="<?= $Admin->id; ?>"
                                        class="btn btn-danger">Delete
                                </button>
                            </form>
                        </td>
                    <?php } ?>
                    </tr>
                    <?php
                } else {
                    echo "No Record Found";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>


    </body>
    </html>
<?php
