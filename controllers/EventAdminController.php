<?php
/**Created by Robin Knol**/
require 'models/EventModel.php';

class EventAdminController extends PagesController
{
    public function update()
    {
        try {
            $eventmodel = new EventAdminModel();
            $eventmodel->update($_POST['id'],
                $_POST['name'],
                $_POST['description'],
                $_POST['locatie']);

        } catch (Exception $e) {
            die('er is iets fout gegaan');
            //header 500 pagina
        }

        return header("Location: adminEvent");
    }

    public function create()
    {
        try {
            $eventmodel = new EventAdminModel();
            $eventmodel->create($_POST['name'],
                $_POST['description'],
                $_POST['locatie']);
        } catch (Exception $e) {
            die('er is iets fout gegaan');
            //header 500 pagina
        }


        return header("Location: adminEvent");
    }

    public function delete()
    {
        $eventmodel = new EventAdminModel();
        $eventmodel->delete($_POST['id']);
        return header("Location: adminEvent");
    }
}

