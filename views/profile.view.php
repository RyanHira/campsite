<?php
require 'inc/nav.php';
/**made by Jaymian Nawang**/

?>
<!DOCTYPE html>
<html lang="en">

<body>

<br>
<br>
<br>
<div class="container">
    <!-- Main Content-->
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane"
                    type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Account Gegevens
            </button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane"
                    type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Boekingen
            </button>
        </li>

    </ul>
    <!--    Aanpassen account details-->
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab"
             tabindex="0">
            <p>Pas hier uw account gegevens aan.</p>

            <form class="row g-3">
                <div class="col-12">
                    <label for="inputEmail4" class="form-label">E-mail</label>
                    <input type="email" class="form-control" id="inputEmail4" placeholder="E-mail">
                </div>
                <div class="col-md-6">
                    <label for="inputPassword4" class="form-label">Naam</label>
                    <input type="text" class="form-control" id="inputPassword4" placeholder="Voornaam">
                </div>
                <div class="col-md-6">
                    <label for="inputEmail4" class="form-label">Achternaam</label>
                    <input type="text" class="form-control" id="inputEmail4">
                </div>
                <div class="col-md-6">
                    <label for="inputCity" class="form-label">Straat+Huisnummer</label>
                    <input type="text" class="form-control" id="inputCity">
                </div>
                <div class="col-md-4">
                    <label for="inputZip" class="form-label">Postcode</label>
                    <input type="text" class="form-control" id="inputZip">
                </div>
                <div class="col-md-2">
                    <label for="inputCity" class="form-label">Telefoonnummer</label>
                    <input type="text" class="form-control" id="inputCity">
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-success">Opslaan</button>
                </div>
            </form>
        </div>


        <!--    Aanpassen Boekingen-->
        <div class="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
            <p>Uw Boekingen:</p>
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Booking voor</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Aantal Personen</h6>
                    <p class="card-text">Van/tot</p>
                    <button type="submit" class="btn btn-success " data-bs-toggle="modal"
                            data-bs-target="#exampleModal">Verander
                    </button>
                    <br>
                    <button type="button" class="btn btn-danger">Annuleer</button>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel">Pas uw reservering aan</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <!--                            Content  in model-->

                            <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">Email address</label>
                                <input type="email" class="form-control" id="exampleFormControlInput1"
                                       placeholder="name@example.com">
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">Datum van</label>
                                <input type="date" class="form-control" id="exampleFormControlInput1">
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">Datum tot</label>
                                <input type="date" class="form-control" id="exampleFormControlInput1">
                            </div>

                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Toevoegingen</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>


<div class="container" style="margin-top: 75px;">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <h3>Events</h3>
                    <a href="createBooking" class="btn btn-success">voeg een klant toe</a>
                    <!--                    <a href="admin" class="btn btn-primary">Back</a>-->
                </div>
                <div class="card-body">
                    <table class="table table-users table-bordered table-striped">
                        <thead>
                        <!--                        create a list, showing the data-->
                        <tr>
                            <th>ID</th>
                            <th>Datum_van</th>
                            <th>Datum_Tot</th>
                            <th>mensen</th>
                            <th>caravan</th>
                            <th>tent</th>
                            <th>camper</th>
                            <th>dieren</th>
                            <th>kinderen</th>


                            <th>edit</th>

                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $row = new BookingModel();
                        $result = $row->selectAll();

                        if ($result)
                        {
                        foreach ($result

                        as $row) {
                        ?>

                        <tr>
                            <td><?= $row->id; ?></td>
                            <td><?= $row->Datum_van; ?></td>
                            <td><?= $row->Datum_Tot; ?></td>
                            <td><?= $row->mensen; ?></td>
                            <td><?= $row->caravan; ?></td>
                            <td><?= $row->tent; ?></td>
                            <td><?= $row->camper; ?></td>
                            <td><?= $row->auto; ?></td>
                            <td><?= $row->dieren; ?></td>
                            <td><?= $row->kinderen; ?></td>
                            <td>
                                <!--                                FETCHING EDIT PAGE THROUGH ROUTES.PHP FROM THE ROW WHERE THE ID'S MATCH-->
                                <a href="/editBooking?id=<?= $row->id; ?>" class="btn btn-primary">Edit</a>
                            </td>
                            <td>
                                <!-------------------FORM ACTION CODE FETCHES _POST FUNCTION FROM CODE.VIEW.PHP-->
                                <form action="/deleteBooking" method="POST">
                                    <button type="submit" name="id" value="<?= $row->id; ?>"
                                            class="btn btn-danger">Delete
                                    </button>
                                </form>
                            </td>

                            <?php }
                            } ?>


                            <?php
                            require 'inc/footer.php';
                            ?>

</html>
</tr>

