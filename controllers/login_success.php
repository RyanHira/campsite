<?php
/**
 * Created by: Robin Kaarsgaren 2022
 **/
session_start();


if(isset($_SESSION["email"]) && basename($_SERVER['PHP_SELF']) !== 'session_patcher') {
    header('location: /');
    exit;
}
else{
    //if not logged in, you go back so you can press the login button to open the menu, or register button to open the form
    echo '<div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
    Login
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
    <form class="dropdown-item" method="post">
        <label for="email"></label>
        <input type="text" class="form-control" name="email" placeholder="uw email" required><br/>

        <label for="pwd"></label>
        <input type="password" class="form-control" name="password" placeholder="uw wachtwoord" required><br/>

        <input type="submit" class="field" name="login" value="login">
    </form>
  </ul>
</div>';
    echo '<a class="nav-link" href="#">REGISTREER</a>';

   }