<?php
/**Created by Robin Knol**/
class EventAdminModel
{

    public function __construct()
    {
        $this->conn = Connection::make(App::get('config')['database']);
    }

    public function selectAll()
    {
        $query = "SELECT * FROM events";
        $stmt = Connection::make(App::get('config')['database'])->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $stmt->fetchAll();
    }

    public function get($id)
    {
        $query = "SELECT * FROM events where id = :id";
        $stmt = Connection::make(App::get('config')['database'])->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        return $stmt->fetch();
    }

    public function create($name, $description, $locatie)
    {

        $query = "INSERT INTO events (name, description, locatie, created_at)
             VALUES (:name,:description,:locatie, date(now()))";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':locatie', $locatie);
        $stmt->execute();

    }

    public function update($id, $name, $description, $locatie)
    {
        $query = "UPDATE events SET name=:name, description=:description, locatie=:locatie WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':locatie', $locatie);
        $stmt->execute();

    }

    public function delete($id)
    {

        $query = "DELETE FROM events WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

    }

}
