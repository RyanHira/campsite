<?php
/**Created by Robin Knol**/
require './models/AdminModel.php';

class AdminController extends PagesController
{
//    update function with post requests to the userModel
    public function update()
    {
        try {
//            creating the userModel
            $AdminModel = new AdminModel();
//            post requests for the different parameters, these are bound by the userModel
            $AdminModel->update($_POST['id'],
                $_POST['adminName'],
                $_POST['pwd'],
                $_POST['adminEmail']);

        } catch (Exception $e) {
//            error handling
            die('er is iets fout gegaan');
            //header 500 pagina
        }
//          redirect back to page.
        return header("Location: showAdmin");
    }

    public function create()
    {
        try {
            //            creating the userModel
            $AdminModel = new AdminModel();
            //            post requests for the different parameters, these are bound by the userModel
            $AdminModel->create($_POST['id'],
                $_POST['adminName'],
                $_POST['pwd'],
                $_POST['adminEmail']);
        } catch (Exception $e) {
//            error handling
            die('er is iets fout gegaan');
            //header 500 pagina
        }
//          redirect back to page.
        return header("Location: showAdmin");

    }

    public function delete()
    {
        //            creating the userModel
        $AdminModel = new AdminModel();
        //            post requests for the different parameters, these are bound by the userModel
        $AdminModel->delete($_POST['id']);
        return header("Location: showAdmin");
    }
}