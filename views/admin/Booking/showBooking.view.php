<?php
/**created by Jaymian Nawang and Robin Knol**/
include('./views/inc/footer.php');
include('./views/inc/navAdmin.php');

?>

<!--standard container bootstrap-->
<div class="container" style="margin-top: 75px; margin-bottom: 200px;">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <h3>Camping Data</h3>
                    <!--                        button to go to add klant php page-->
                    <a href="/createBookingAdmin" class="btn btn-success">voeg een booking toe toe</a>
                    <a href="/admin" class="btn btn-primary">Back</a>
                </div>
                <div class="card-body">
                    <table class="table table-users table-bordered table-striped">
                        <thead>
                        <!--                        create a list, showing the data-->
                        <tr>
                            <th>ID</th>
                            <th>datum van</th>
                            <th>datum tot</th>
                            <th>mensen</th>
                            <th>caravan</th>
                            <th>tent</th>
                            <th>camper</th>
                            <th>auto</th>
                            <th>dieren</th>
                            <th>kinderen</th>
                            <th>edit</th>

                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $booking = new BookingAdminModel();
                        //----------------------FOREACH LOOP INTO $ROW TO SHOW EACH DATABASE ITEM AS $ROW->XX----------------------------------
                        $result = $booking->selectAll();
                        if ($result) {
                            foreach ($result as $row) {
                                ?>
                                <tr>
                                    <td><?= $row->id; ?></td>
                                    <td><?= $row->Datum_van; ?></td>
                                    <td><?= $row->Datum_Tot; ?></td>
                                    <td><?= $row->mensen; ?></td>
                                    <td><?= $row->caravan; ?></td>
                                    <td><?= $row->tent; ?></td>
                                    <td><?= $row->camper; ?></td>
                                    <td><?= $row->auto; ?></td>
                                    <td><?= $row->dieren; ?></td>
                                    <td><?= $row->kinderen; ?></td>
                                        <td>
                                            <!--                                FETCHING EDIT PAGE THROUGH ROUTES.PHP FROM THE ROW WHERE THE ID'S MATCH-->
                                            <a href="/editBookingAdmin?id=<?= $row->id; ?>" class="btn btn-primary">Edit</a>
                                        </td>
                                        <td>
                                            <!-------------------FORM ACTION CODE FETCHES _POST FUNCTION FROM CODE.VIEW.PHP-->
                                            <form action="/deleteBooking" method="POST">
                                                <button type="submit" name="id" value="<?= $row->id; ?>"
                                                        class="btn btn-danger">Delete
                                                </button>
                                            </form>
                                        </td>
                                    <?php }
                            }?>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
