<?php
/**created by robin knol**/
include('./views/inc/footer.php');
include('./views/inc/navAdmin.php');
?>
<body>


<!--standard container bootstrap-->
<div class="container" style="margin-top: 75px; margin-bottom: 200px;">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header text-center">
                    <h3>Camping Data</h3>
                    <div class="card-body">
                        <a href="/adminUser" class="btn btn-primary">manage users</a>
                        <a href="/adminEvent" class="btn btn-primary">manage events</a>
                        <a href="/adminBooking" class="btn btn-primary">manage Booking</a>
                        <a href="/showAdmin" class="btn btn-primary">manage admin accounts</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>