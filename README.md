## About the camping
### Installation

First unpack the website using 7zip or winrar (or any other alternative that supports it) and put it in the desired directory.

alternatively for more advanced users follow the following steps:
- cloning the project
    1. Head to the directory you wish to store the project, in the directory URL type "cmd" and hit enter, or, if you have bash installed right mouse button on the empty folder and press "git bash here".
    2. type/copy the command:<br>
       ```git clone https://gitlab.com/RyanHira/campsite.git```

## How to run the project
go to config.php and change the password, the database we used is called "camping".
grab a terminal and type/paste the following command:

```php -S localhost:8000```


## How to use

**first make an admin account by inserting everything from SQL.sql**

- **Admin Panel:**
    - Manage Admin accounts.
    - Manage user accounts.
    - Manage booking.
    - Manage events.

---

## Contributors
### This project was made by:


> [**Robin Knol**](https://gitlab.com/RobinKnol)

> [**Robin Kaarsgaren**](https://gitlab.com/RobProgramming)

> [**Ryan Hira**](https://gitlab.com/RyanHira)

> [**Jaymian Nawang**](https://gitlab.com/jayfr)