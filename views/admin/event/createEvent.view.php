<?php

/**created by Ryan Hira and Robin Knol**/
include('./views/inc/footer.php');
include('./views/inc/navAdmin.php');
?>
<div class="container" style="margin-top: 75px; margin-bottom: 200px;">

    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <h3>Event Toevoegen</h3>
                    <a href="adminEvent" class="btn btn-primary">Back</a>
                </div>
                <div class="card-body">
                    <h1>Maak een event aan!</h1>
                    <form action="/createEvent" method="POST">
                        <div class="mb-3">
                            <label for="name" class="form-label">EventName</label>
                            <input type="text" name="name" required class="form-control" id="name">
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">Description</label>
                            <input type="text" name="description" required class="form-control" id="description">
                        </div>
                        <div class="mb-3">
                            <label for="locatie" class="form-label">Locatie</label>
                            <input type="text" name="locatie" required class="form-control" id="locatie">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
