<?php
/**created by robin knol and Robin Kaarsgaren**/
include('./views/inc/footer.php');
include('./views/inc/navAdmin.php');
?>
<div class="container" style="margin-top: 75px; margin-bottom: 200px;">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <h3>Camping Data</h3>
                    <a href="/admin" class="btn btn-primary">Back</a>
                </div>
                <div class="card-body">
                    <h1>User update</h1>
                    <?php
                    $userModel = new UserModel();
                    $currentUser = $userModel->get($_GET['id']);
                    ?>
                    <form action="/updateUser" method="POST">
                        <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
                        <div class="mb-3">
                            <label for="voornaam" class="form-label">voornaam</label>
                            <input type="text" name="voornaam" required class="form-control" id="voornaam"
                                   value="<?= $currentUser->voornaam ?>">
                        </div>
                        <div class="mb-3">
                            <label for="achternaam" class="form-label">achternaam</label>
                            <input type="text" name="achternaam" required class="form-control"
                                   id="achternaam" value="<?= $currentUser->achternaam ?>">
                        </div>
                        <div class="mb-3">
                            <label for="pwd" class="form-label">Postcode</label>
                            <input type="password" name="pwd" required class="form-control" id="pwd"
                                   value="<?= $currentUser->pwd ?>">
                        </div>
                        <div class=" mb-3">
                            <label for="telnummer" class="form-label">Straat en huisnummer</label>
                            <input type="text" name="telnummer" required class="form-control" id="telnummer"
                                   value="<?= $currentUser->telnummer ?>">
                        </div>
                        <div class=" mb-3">
                            <label for="email" class="form-label">Email address</label>
                            <input type="email" name="email" required class="form-control" id="email"
                                   aria-describedby="emailHelp" value="<?= $currentUser->email ?>">
                            <div id=" emailHelp" class="form-text">We'll never share your email with anyone else.
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
