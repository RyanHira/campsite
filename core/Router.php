<?php

/**
 * Created by: Stephan Hoeksema 2022
 * Editted by Robin Knol 2023
 * untitled
 */
class Router
{
    //    protected routes
    protected $routes = [];

    public static function load($file)
    {
        $router = new static;
        require $file;
        return $router;
    }

    //  methode define
    public function define($routes)
    {
        $this->routes = $routes;
    }

    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    public function post($uri, $controller)
    {
        $this->routes['POST'][$uri] = $controller;
    }

    //    Robin Knol made some changes to the direct function so that uri without a query sting can be read.
    //  method direct
    public function direct($uri, $requestType)
    {
        $uriWithoutQueryString = strtok($uri, '?');


        if (array_key_exists($uriWithoutQueryString, $this->routes[$requestType])) {

            return $this->callAction(...explode('@', $this->routes[$requestType][$uriWithoutQueryString]));
        }
        if (array_key_exists($uri, $this->routes[$requestType])) {
            $this->routes[$requestType][$uri];
            return $this->callAction(...explode('@', $this->routes[$requestType][$uri]));
        }
        throw new Exception('Route not defined');
    }

    protected function callAction($controller, $action)
    {
        $controller = new $controller;
        if (!method_exists($controller, $action)) {
            throw new Exception('Method does not exist.');
        }
        return $controller->$action();
    }
}








