<?php
/**created by robin knol**/
require 'models/UserModel.php';

class UserController extends PagesController
{

//    update function with post requests to the userModel
    public function update()
    {
        try {
//            creating the userModel
            $usermodel = new UserModel();
//            post requests for the different parameters, these are bound by the userModel
            $usermodel->update($_POST['id'],
                $_POST['voornaam'],
                $_POST['achternaam'],
                $_POST['pwd'],
                $_POST['telnummer'],
                $_POST['email']);


        } catch (Exception $e) {
//            error handling
            die('er is iets fout gegaan');
            //header 500 pagina
        }
//          redirect back to page.
        return header("Location: adminUser");
    }

    public function create()
    {
        try {
            //            creating the userModel
            $usermodel = new UserModel();
            //            post requests for the different parameters, these are bound by the userModel
            $usermodel->create(
                $_POST['voornaam'],
                $_POST['achternaam'],
                $_POST['pwd'],
                $_POST['telnummer'],
                $_POST['email']);
        }
 catch (Exception $e) {
//            error handling
            die('er is iets fout gegaan');
            //header 500 pagina
        }
//          redirect back to page.
        return header("Location: adminUser");

  }

    public function delete()
    {
        //            creating the userModel
        $usermodel = new UserModel();
        //            post requests for the different parameters, these are bound by the userModel
        $usermodel->delete($_POST['id']);
        return header("Location: adminUser");
    }
}

