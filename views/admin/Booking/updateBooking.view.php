<?php
/**created by Jaymian Nawang and Robin Knol**/
include('./views/inc/footer.php');
include('./views/inc/navAdmin.php');
?>
<div class="container" style="margin-top: 75px; margin-bottom: 200px;">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <h3>Camping Data</h3>
                    <a href="/adminBooking" class="btn btn-primary">Back</a>
                </div>
                <div class="card-body">
                    <h1>User update</h1>
                    <?php
                    $BookingModel = new BookingModel();
                    $CurrentBooking = $BookingModel->get($_GET['id']);
                    ?>
                    <form action="/updateBooking" method="POST">
                        <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
                        <div class="mb-3">
                            <label for="Datum_van" class="form-label">voornaam</label>
                            <input type="text" name="Datum_van" required class="form-control" id="Datum_van"
                                   value="<?= $CurrentBooking->Datum_van ?>">
                        </div>
                        <div class="mb-3">
                            <label for="Datum_tot" class="form-label">achternaam</label>
                            <input type="text" name="Datum_tot" required class="form-control"
                                   id="Datum_tot" value="<?= $CurrentBooking->Datum_Tot ?>">
                        </div>
                        <div class="mb-3">
                            <label for="mensen" class="form-label">Postcode</label>
                            <input type="text" name="mensen" required class="form-control" id="mensen"
                                   value="<?= $CurrentBooking->mensen ?>">
                        </div>
                        <div class=" mb-3">
                            <label for="caravan" class="form-label">Straat en huisnummer</label>
                            <input type="text" name="caravan" required class="form-control" id="caravan"
                                   value="<?= $CurrentBooking->caravan ?>">
                        </div>

                        <div class=" mb-3">
                            <label for="tent" class="form-label">Telefoon Nummer</label>
                            <input type="Phone" name="tent" required class="form-control" id="tent"
                                   value="<?= $CurrentBooking->tent ?>">
                        </div>
                        <div class=" mb-3">
                            <label for="camper" class="form-label">Straat en huisnummer</label>
                            <input type="text" name="camper" required class="form-control" id="camper"
                                   value="<?= $CurrentBooking->camper ?>">
                        </div>
                        <div class=" mb-3">
                            <label for="auto" class="form-label">Straat en huisnummer</label>
                            <input type="text" name="auto" required class="form-control" id="auto"
                                   value="<?= $CurrentBooking->auto ?>">
                        </div>
                        <div class=" mb-3">
                            <label for="dieren" class="form-label">Straat en huisnummer</label>
                            <input type="text" name="dieren" required class="form-control" id="dieren"
                                   value="<?= $CurrentBooking->dieren ?>">
                        </div>
                        <div class=" mb-3">
                            <label for="kinderen" class="form-label">Straat en huisnummer</label>
                            <input type="text" name="kinderen" required class="form-control" id="kinderen"
                                   value="<?= $CurrentBooking->kinderen ?>">
                        </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
