<?php
/**
 * Created by: Robin Kaarsgaren 2022
 **/
require 'inc/nav.php';
include 'controllers/RegisterController.php';
$register = new RegisterController();

if(isset($_POST['register'])) {
    $register->register();
}
?>
<!DOCTYPE html>
<html lang="en">

<body>
<!-- Page Header-->
<header class="masthead" style="background-image: url('../images/head.jpg')">
    <div class="container position-relative px-4 px-lg-5" ">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <div class="site-heading">
                    <h1>Camping</h1>
                    <span class="subheading">De beste camping van Nederland! </span>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main Content-->
<div class="container px-4 px-lg-5" style="margin-bottom: 200px";>
    <form method="post" class="row">
        <div class="col-12">
            <label for="voornaam">Voornaam:</label>
            <input type="text" id="voornaam" name="voornaam" class="form-control" required>
        </div>
        <div class="col-12">
            <label for="achternaam">Achternaam:</label>
            <input type="text" id="achternaam" name="achternaam" class="form-control" required>
        </div>
        <div class="col-12">
            <label for="pwd">Wachtwoord:</label>
            <input type="password" id="pwd" name="pwd" class="form-control" required>
        </div>
        <div class="col-12">
            <label for="telnummer">Telefoon nummer: (zonder spatie)</label>
            <input type="tel" id="telnummer" name="telnummer" class="form-control" required>
        </div>
        <div class="col-12">
            <label for="email">E-mail:</label>
            <input type="email" id="email" name="email" class="form-control" required>
        </div>
        <div class="col-12">
            <input type="submit" name="register" value="register" class="btn btn-primary">
        </div>
    </form>



</div>

</body>

<?php
require 'inc/footer.php';
?>
</html>
