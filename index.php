<?php
require 'vendor/autoload.php';
require 'core/support.php';

App::bind('config', require 'config.php');
App::bind('query', Connection::make(App::get('config')['database']));
Router::load('routes.php')->direct(Request::uri(), Request::method()); //chain methods