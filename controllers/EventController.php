<?php
/**made by Ryan Hira**/

require 'models/EventModel.php';

class EventController extends PagesController
{
    public function update()
    {
        try {
            $eventmodel = new EventModel();
            $eventmodel->update($_POST['id'],
                $_POST['name'],
                $_POST['description'],
                $_POST['locatie']);

        } catch (Exception $e) {
            die('er is iets fout gegaan');
            //header 500 pagina
        }

        return header("Location: adminEvent");
    }

    public function create()
    {
        try {
            $eventmodel = new EventModel();
            $eventmodel->create($_POST['name'],
                $_POST['description'],
                $_POST['locatie']);
        } catch (Exception $e) {
            die('er is iets fout gegaan');
            //header 500 pagina
        }


        return header("Location: adminEvent");
    }

    public function delete()
    {
        $eventmodel = new EventModel();
        $eventmodel->delete($_POST['id']);
        return header("Location: adminEvent");
    }
}

