<?php
/**
 * Created by: Robin Kaarsgaren 2022
 **/
session_destroy();
session_start();
try {
    //checks for empty fields
    if(isset($_POST["login"])){
        if(empty($_POST["adminName"]) || empty($_POST["pwd"])){
            $message = '<label>Alle velden moeten worden ingevult!</label>';
        }
        //if fields are populated the database compares input with rows
        else{
            $query = "SELECT * FROM admin WHERE adminName = :adminName AND pwd = :pwd";
            $stmt = Connection::make(App::get('config')['database'])->prepare($query);
            $stmt->execute(
                array(
                    'adminName' => $_POST["adminName"],
                    'pwd' => $_POST["pwd"]
                )
            );
            //if success a session starts
            $count = $stmt->rowCount();
            if($count > 0){
                $_SESSION["adminName"] = $_POST["adminName"];
                header("location:loginSuccessAdmin");
            }
            //if fails, gives error message about the credentials provided
            else{
                $message = '<label>gegevens kloppen of bestaan niet</label>';
            }
        }
    }
}
//catch error message
catch(PDOException $error){
    $message = $error->getMessage();
}

