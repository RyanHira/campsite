<?php

/**
 * Created by: Stephan Hoeksema 2022
 * wfflix-cd
 * option+command+L voor windows = control+alt+L voor uitlijnen code
 */
class Connection
{
    public static function make($config)
    {
        try {

            return new PDO($config['connection'] . ';dbname=' . $config['name'],
                $config['user'],
                $config['pw'],
                $config['options']);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}