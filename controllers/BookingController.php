<?php
require 'models/BookingModel.php';

class BookingController extends PagesController

{
    public function update()
    {
        try {
            $bookingmodel = new BookingModel();
            $bookingmodel->update($_POST['id'],
                $_POST['Datum_van'],
                $_POST['Datum_tot'],
                $_POST['mensen'],
                $_POST['caravan'],
                $_POST['tent'],
                $_POST['camper'],
                $_POST['auto'],
                $_POST['dieren'],
                $_POST['kinderen']);

        } catch (Exception $e) {
            die('er is iets fout gegaan');
            //header 500 pagina
        }

        return header("Location: post");
    }

    public function create()
    {
       try {
            $bookingmodel = new BookingModel();
            $bookingmodel->create(
                $_POST['Datum_van'],
                $_POST['Datum_tot'],
                $_POST['mensen'],
                $_POST['caravan'],
                $_POST['tent'],
                $_POST['camper'],
                $_POST['auto'],
                $_POST['dieren'],
                $_POST['kinderen']
          );
    } catch (Exception $e) {
           die('er is iets fout gegaan');
       }




       return header("Location: post");
   }


    public function delete()
    {
        $bookingmodel = new BookingModel();
        $bookingmodel->delete($_POST['id']);
        return header("Location: post");
    }


}