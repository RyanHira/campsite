<?php
/**
 * Created by: Robin Kaarsgaren 2022
 **/

class SessionV2
{
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function login()
    {
        try {
            //checks for empty fields
            if (isset($_POST["login"])) {
                if (empty($_POST["email"]) || empty($_POST["pwd"])) {
                    $message = '<label>Alle velden moeten worden ingevult!</label>';
                } //if fields are populated the database compares input with rows
                else {
                    $query = "SELECT id, voornaam, achternaam, email, telnummer, pwd FROM users WHERE email = :email";
                    $stmt = Connection::make(App::get('config')['database'])->prepare($query);
                    $stmt->execute(
                        array(
                            'email' => $_POST["email"]
                        )
                    );
                    //if success a session starts
                    $count = $stmt->rowCount();
                    if ($count > 0) {
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        if (password_verify($_POST["pwd"], $row["pwd"])) {
                            $_SESSION["id"] = $row["id"];
                            $_SESSION["voornaam"] = $row["voornaam"];
                            $_SESSION["achternaam"] = $row["achternaam"];
                            $_SESSION["email"] = $row["email"];
                            $_SESSION["telnummer"] = $row["telnummer"];
                            header("location:loginSuccess");
                        } else {
                            $message = '<label>gegevens kloppen of bestaan niet</label>';
                        }
                    } //if fails, gives error message about the credentials provided
                    else {
                        $message = '<label>gegevens kloppen of bestaan niet</label>';
                    }
                }
            }
        } catch (PDOException $error) {
            $message = $error->getMessage();
        }
    }


    public function check() {
        if(isset($_SESSION["email"])) {
            echo '<li class="nav-link">welkom '.$_SESSION["voornaam"].' '.$_SESSION["achternaam"].'</li>';
            echo '<a class="nav-link btn " href="#">Mijn Account</a>'; //account page link here
            echo '<a class="nav-link btn " href="./logout.php">Log uit</a>';
        } else {
            echo '<div class="btn-group">
                <div class="btn-group dropstart" role="group">
                    <a type="button" class="btn dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                        <span >Login</span>
                    </a>
                    <ul class="dropdown-menu" style="width: 400px" aria-labelledby="dropdownMenuButton1">
                        <form class="col" method="post" action=""';
            echo $_SERVER["PHP_SELF"];
            echo '">
                            <label for="email">Uw e-mailadres</label>
                            <input type="text" class="form-control" name="email" placeholder="uw e-mail" required><br/>
                            <label for="pwd">Uw wachtwoord</label>
                            <input type="password" class="form-control" name="pwd" placeholder="uw wachtwoord" required><br/>
                            <input type="submit" class="field" name="login" value="login">
                        </form>
                    </ul>
                </div>';
            echo '<a class="nav-link" href="registerPage">Registreer</a>';
        }
    }
}

$session = new SessionV2();

if (isset($_POST["login"])) {
    $session->login();
}

$session->check();