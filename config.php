<?php
return [
    'database' => [
        'name' => 'camping', //database name
        'user' => 'root', //username
        'pw' => 'T1i2m3e4', //password for the database
        'connection' => 'mysql:host=127.0.0.1',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ]
    ]
];