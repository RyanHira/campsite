<?php return array(
    'root' => array(
        'name' => '__root__',

        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'ea1e37879ca06f6ce3cf76a94c51961118a010d7',

        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => 'cd0ddaa01289646952b5d8e0e5e3d3b6dcd8b8c6',

        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(

            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'ea1e37879ca06f6ce3cf76a94c51961118a010d7',

            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'cd0ddaa01289646952b5d8e0e5e3d3b6dcd8b8c6',

            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
