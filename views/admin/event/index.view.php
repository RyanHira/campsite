<?php
/**created by Ryan Hira**/

include('./views/inc/nav.php');
//include('');


?>

<!--standard container bootstrap-->
<div class="container" style="margin-top: 75px;">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <h3>Events</h3>
                    <a href="createEvent" class="btn btn-success">voeg een klant toe</a>
                    <!--                    <a href="admin" class="btn btn-primary">Back</a>-->
                </div>
                <div class="card-body">
                    <table class="table table-users table-bordered table-striped">
                        <thead>
                        <!--                        create a list, showing the data-->
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Locatie</th>
                            <th>edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $event = new EventModel();
                        //----------------------fOREACH LOOP INTO $ROW TO SHOW EACH DATABASE ITEM AS $ROW->XX----------------------------------
                        $result = $event->selectAll();
                        if ($result) {
                            foreach ($result as $events) {
                                ?>
                                <tr>
                                <td><?= $events->id; ?></td>
                                <td><?= $events->name; ?></td>
                                <td><?= $events->description; ?></td>
                                <td><?= $events->locatie; ?></td>
                                <?php
                                ?>
                                <td>
                                    <!--                                FETCHING EDIT PAGE THROUGH ROUTES.PHP FROM THE ROW WHERE THE ID'S MATCH-->
                                    <a href="/editEvent?id=<?= $events->id; ?>" class="btn btn-primary">Edit</a>
                                </td>
                                <td>
                                    <!-------------------FORM ACTION CODE FETCHES _POST FUNCTION FROM CODE.VIEW.PHP-->
                                    <form action="/deleteEvent" method="POST">
                                        <button type="submit" name="id" value="<?= $events->id; ?>"
                                                class="btn btn-danger">Delete
                                        </button>
                                    </form>
                                </td>
                            <?php } ?>
                            </tr>
                            <?php
                        } else {
                            echo "No Record Found";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
