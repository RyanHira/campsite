<?php
/**Created by Robin Knol**/
require 'models/BookingAdminModel.php';

class BookingAdminController extends PagesController

{
    public function update()
    {
        try {
            $bookingAdminmodel = new BookingAdminModel();
            $bookingAdminmodel->update($_POST['id'],
                $_POST['Datum_van'],
                $_POST['Datum_tot'],
                $_POST['mensen'],
                $_POST['caravan'],
                $_POST['tent'],
                $_POST['camper'],
                $_POST['auto'],
                $_POST['dieren'],
                $_POST['kinderen']);

        } catch (Exception $e) {
            die('er is iets fout gegaan');
            //header 500 pagina
        }

        return header("Location: adminBooking");
    }

    public function create()
    {
        try {
            $bookingAdminmodel = new BookingModel();
            $bookingAdminmodel->create($_POST['Datum_van'],
                $_POST['Datum_Tot'],
                $_POST['mensen'],
                $_POST['caravan'],
                $_POST['tent'],
                $_POST['camper'],
                $_POST['auto'],
                $_POST['dieren'],
                $_POST['kinderen']
            );
        } catch (Exception $e) {
            die('er is iets fout gegaan');
//            //header 500 pagina
  }


        return header("Location: adminBooking");
    }

    public function delete()
    {
        $bookingAdminmodel = new BookingAdminModel();
        $bookingAdminmodel->delete($_POST['id']);
        return header("Location: adminBooking");
    }


}