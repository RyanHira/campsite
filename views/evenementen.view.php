<?php
/**made by Ryan hira**/
require 'inc/nav.php';
?>
<!DOCTYPE html>
<html lang="en">

<body>
<!-- Page Header-->
<header class="masthead" style="background-image: url('../images/event.jpg')">
    <div class="container position-relative px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <div class="site-heading">
                    <h1>Evenementen</h1>
                    <span class="subheading">De beste camping van Nederland! </span>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="container px-4 px-lg-5" style="margin-bottom: 200px;">

    <!-- Main Content-->
    <div class="container px-4 px-lg-5">

        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">

                <h1>Evenementen</h1>

                <!-- Main Content-->
                <?php
                $event = new EventModel();

                $result = $event->selectAll();

                if ($result)


                {
                foreach ($result

                as $events)
                {
                ?>

                <!-- Post preview-->
                <div class="post-preview">
                    <a href="eventsDetail?id=<?= $events->id; ?>">
                        <h2 class="post-title"><?= $events->name; ?></h2>
                        <h3 class="post-subtitle"> <?= $events->locatie; ?></h3>
                        <p class="post-meta">
                            Lees meer
                        </p>
                    </a>



                    <?php } ?>


                    <?php

                    //                        }
                    }
                    else {
                        echo "No Record Found";
                    }
                    //                                ?>


                </div>
            </div>
        </div>

</body>

<?php
require 'inc/footer.php';
?>
</html>
