<?php
/**made by Robin Knol**/
session_destroy();
require 'inc/nav.php';
require 'controllers/login-admin.php';
?>
<!DOCTYPE html>
<html lang="en">

<body>
<!-- Page Header-->
<header class="masthead" style="background-image: url('../images/head.jpg')">
    <div class="container position-relative px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <div class="site-heading">
                    <h1>Camping</h1>
                    <span class="subheading">De beste camping van Nederland! </span>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main Content-->
<!----------login for admin---------->
<div class="d-flex justify-content-center flex-mb-4" style="margin-bottom: 200px;">

    <!-- login for admins -->
    <form method="post">
        <h1>Admin login</h1>
        <?php if(isset($message)){
            echo '<label class="text-danger">'.$message.'</label></br>';
        } ?>
        <label for="adminName">Uw gebruikersnaam:</label>
        <input type="text" class="form-control" name="adminName" id="adminName" placeholder="Vul hier uw gebruikersnaam in" required><br/>

        <label for="pwd">Uw wachtwoord:</label>
        <input type="password" class="form-control" name="pwd" id="pwd" placeholder="Vul hier uw wachtwoord in" required><br/>

        <input type="submit" class="field" name="login" value="login">
    </form>
</div>


</div>

</body>

<?php
require 'inc/footer.php';
?>
</html>






